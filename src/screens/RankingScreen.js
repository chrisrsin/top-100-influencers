import React, {useEffect, useState} from 'react';
import { Text, SafeAreaView, StyleSheet } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import RankedItem from '../components/RankedItem';
import LoadingOverlay from "../components/LoadingOverlay";
import {getTopIgFollowers} from "../utils/http";

import {transformIgResponse} from "../utils/TransformRequestData";


const RankingScreen = ({route, navigation}) => {
  const [rankedData, setRankedData] = useState();
  const [isFetching, setIsFetching] = useState(true);


  useEffect(() => {
    async function getRankedResults() {
      
      setIsFetching(true);
      const igResults = (await getTopIgFollowers()).data;
      setIsFetching(false);
      setRankedData(transformIgResponse(igResults));        
    }
    getRankedResults();
  }, []);

  if (isFetching) {
    return <LoadingOverlay />
  }

  const renderRankedItem = ({item}) => (
    <RankedItem
      title={item.username}
      image={item.image}
      description={item.occupation}
      followers={item.followers}
      color="#4267B2"
      textColor="#ddebfd"
      rank={item.rank}
      profilePicUrl={item.profileImageResponse}
    />

  );
    
    return (
      <SafeAreaView style = {{flex: 1, backgroundColor: "#fff"}}>
      <Text style = {styles.title}>Rankings</Text>
        <FlatList
          data = {rankedData}
          renderItem = {renderRankedItem}
          keyExtractor = {item => item.rank}
        />
    </SafeAreaView>
    );
  }

  const styles = StyleSheet.create({
    root: {
      justifyContent: "center",
      alignItems: "center",
    },
    title: {
      width: "100%",
      marginTop: 60,
      fontSize: 25,
      fontWeight: "bold",
      textAlign: "center"
    },
    pfp: {
      height: 50,
      width: 50,
    },
    container: {
      flex: 1,
      backgroundColor: "#fff",
    },
    userWrapper: {
      flexDirection: "row",
      justifyContent: "space-around",
      width: "100%",
      marginVertical: 20,
    },
    rootContainer: {
      marginTop: 24,
      marginBottom: 24
    },
  });

  export default RankingScreen;