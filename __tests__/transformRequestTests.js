import React from "react";
import { getTopIgFollowers } from "../src/utils/http";
import { transformFacebookResponse, transformIgResponse, transformSpotifyResponse, transformTiktokResponse, transformTwitchResponse, transformTwitterResponse, transformTwitterUsername, transformYoutubeResponse } from "../src/utils/TransformRequestData";



test('Transorms Instagram API Call Response to a JSON format', () => {
    const mockData = [
        [
            [],
            [
                "1",
                "@instagram",
                "Instagram",
                "504",
                "Social media platform",
                "United States"
            ],
            []
        ]
    ];

    const transformedResponse = transformIgResponse(mockData);
    expect(transformedResponse[0].rank).toBe("1");
    expect(transformedResponse[0].username).toBe("@instagram");
    expect(transformedResponse[0].name).toBe("Instagram");
    expect(transformedResponse[0].followers).toBe("504");
    expect(transformedResponse[0].occupation).toBe("Social media platform");
});

test('Transorms Twitter API Call Response to a JSON format', () => {
    const mockData = [
        [
            [],
            [
                "1",
                "",
                "@BarackObama",
                "Barack Obama",
                "131.9",
                "44th President of the United States of America",
                "United States"
            ],
            []
        ]
    ];

    const transformedResponse = transformTwitterResponse(mockData);
    expect(transformedResponse[0].rank).toBe(1);
    expect(transformedResponse[0].username).toBe("@BarackObama");
    expect(transformedResponse[0].name).toBe("Barack Obama");
    expect(transformedResponse[0].followers).toBe("131.9");
    expect(transformedResponse[0].occupation).toBe("44th President of the United States of America");
});

test('Transorms Tik Tok API Call Response to a JSON format', () => {
    const mockData = [
        [
            [],
            [
                "1",
                "@charlidamelio",
                "Charli D'Amelio",
                "140.3",
                "10828.5",
                "Dancer and social media personality",
                "United States",
                "—"
            ],
            []
        ]
    ];

    const transformedResponse = transformTiktokResponse(mockData);
    expect(transformedResponse[0].rank).toBe(1);
    expect(transformedResponse[0].username).toBe("Charli D'Amelio");
    expect(transformedResponse[0].name).toBe("140.3");
    expect(transformedResponse[0].occupation).toBe("Dancer and social media personality");
});


test('Transorms Spotify API Call Response to a JSON format', () => {
    const mockData = [
        [
            [],
            [
                "1",
                "Ed Sheeran",
                "96.78",
                "United Kingdom",
                "[4]"
            ],
            []
        ]
    ];

    const transformedResponse = transformSpotifyResponse(mockData);
    expect(transformedResponse[0].rank).toBe(1);
    expect(transformedResponse[0].username).toBe("Ed Sheeran");
    expect(transformedResponse[0].name).toBe("Ed Sheeran");
    expect(transformedResponse[0].followers).toBe("96.78");
    expect(transformedResponse[0].occupation).toBe("United Kingdom");
});

test('Transorms YouTube API Call Response to a JSON format', () => {
    const mockData = [
        [
            [],
            [
                "1",
                "T-Series",
                "Link",
                "213",
                "Hindi[11][12]",
                "Music",
                "India",
                ""
            ],
            []
        ]
    ];

    const transformedResponse = transformYoutubeResponse(mockData);
    expect(transformedResponse[0].rank).toBe(1);
    expect(transformedResponse[0].username).toBe("T-Series");
    expect(transformedResponse[0].name).toBe("Link");
    expect(transformedResponse[0].followers).toBe("213");
    expect(transformedResponse[0].occupation).toBe("Music");
});

test('Transorms Spotify API Call Response to a JSON format', () => {
    const mockData = [
        [
            [],
            [
                "1",
                "Ed Sheeran",
                "96.78",
                "United Kingdom",
                "[4]"
            ],
            []
        ]
    ];

    const transformedResponse = transformSpotifyResponse(mockData);
    expect(transformedResponse[0].rank).toBe(1);
    expect(transformedResponse[0].username).toBe("Ed Sheeran");
    expect(transformedResponse[0].name).toBe("Ed Sheeran");
    expect(transformedResponse[0].followers).toBe("96.78");
    expect(transformedResponse[0].occupation).toBe("United Kingdom");
});

test('Transorms Twitch API Call Response to a JSON format', () => {
    const mockData = [
        [
            [],
            [
                "1",
                "Ninja",
                "Richard Blevins",
                "18.2",
                "Fortnite, League of Legends",
                "—"
            ],
            []
        ]
    ];

    const transformedResponse = transformTwitchResponse(mockData);
    expect(transformedResponse[0].rank).toBe(1);
    expect(transformedResponse[0].username).toBe("Ninja");
    expect(transformedResponse[0].name).toBe("Richard Blevins");
    expect(transformedResponse[0].occupation).toBe("Fortnite, League of Legends");
});

test("Update Object to get Twitter username", () => {
    expect(transformTwitterUsername("@top100influencers")).toBe("top100influencers");
})