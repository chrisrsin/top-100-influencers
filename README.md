# Top 100 Influencers

Top 100 Influencers is a Senior Project designed by the Herky Hackers.

## Team Members
1. Christopher Sin
2. Inigo Jaque
3. July Pham
4. Gerardo Bolanos
5. Amit Kanotra
6. Yared Engida

## Project Description
The ultimate goal is for this app to have users to stay up to date with who the top influencers are for each entertainment category.
This application is one that aggregates different social media applciations from top trending influencers into one
  
The Top 100 Influencers mobile application enables the users achieve the following
- maintain tabs on your favorite influencers across the wide variety of social media platforms
- view trending posts
- view trending post per social media category
- view top influencers per category
- select influencer profile and view their latest tweeets

People utilize multiple social media platforms in order to interact with either each other or with their favorite influencers. This interaction can be done in one place in order to have an organized experience.


## Tech Stack
Client: React Native
Server: Expo Client  
Database: Firebase NoSQL Cloud Real Time Database and Authentication Server  

## Code
When developing our application, we decided to follow the React industry standard and chose to build our components through JavaScript arrow functions that return a JSX object. This approach makes it a lot easier to maintain each others changes as the components end up being relatively small blocks of code that can be reused anywhere in our application.
For example, we were able to create from input fields that can be reused on numerous types of screens.  
  
The example below demonstrates our form fields that are seen in our Login and Sign Up page.
```
const FormInput = ({textInputLabel, placeHolderText, iconType, ...props}) => {

    return (
        <View style={styles.inputContainer}>
            <View style={styles.iconStyle}>
                <FontAwesome5Icon name={iconType} size={25} color="#0f52b9" />
            </View>
            <TextInput
                numberOfLines={1}
                value={textInputLabel}
                placeholder={placeHolderText}
                placeholderTextColor="#6ea2f3"
                styles={styles.input}
                {...props}
            />
        </View>
    );
}
``` 

# Downloading and Setting Up Development Environment
## Prerequesites
In order to download the project locally on your machine, you will want follow the prerequisite steps defined below.  
Before you can begin development, you will want to make sure you have the necessary tools and packages installed on your machine
* node npm or yarn
* expo cli

If you do not have the expo cli installed yet, and you already installed npm, you can isntall the expo-cli with the following command
```
npm i -g expo-cl
```

Additionally to this, if you are running on a Mac, you are going to want to install XCode in order to run the project through an IOS simulator. Otherwise, if you want to run the project on an Android simulator make sure to have Android Studio installed on your machine.

## Cloning and Running the Project
1. Clone the project using the command line or your IDE terminal
```
https://gitlab.com/chrisrsin/top-100-influencers.git
```
2. Next install all the modules the project depends on
```
npm install
```
3. Then make sure to cd into the folder and run the project. This step will open a webpage control panel where you can proceed with selecting simulated device you'd like the project to run.
```
cd top100influencers
expo start
```

Alternatively, you also have the option to run the project on a specific simulated device directly from the command line  
  
For iOS
```
expo run:ios
```
For Android
```
expo run:android
```
# Documentation
## User Manual
To understand functionality and features, feel free to go over our user manual documenation  

[User Manual](https://docs.google.com/document/d/1qCfJDXdkIf0469BFlVfRj3j2vvkQtUBMnXUHbm1cjrw/edit?usp=sharing)

## Maintenance Manual
To use as a reference if you wish to learn how the code was structure in order to maintain the code and its deployment  

[Maintenance Manual](https://docs.google.com/document/d/1DnmVqNFGzRePYZafDNRGfm9x1AA0U9VX/edit?usp=sharing&ouid=116672064887999106426&rtpof=true&sd=true)

## System Test Report
View test report for information on how to run unit test designed for this application  

[System Test Report](https://docs.google.com/document/d/1ub9XWPrimW3Z1TGtZfvhZTz09RTrimWG/edit?usp=sharing&ouid=116672064887999106426&rtpof=true&sd=true)

## Product Delivery
Final documentations verifying project deliver  

[Product Delivery](https://docs.google.com/document/d/1SM840DVLwQXwnSr5JBB-_0Zj3lZ6ORBf/edit?usp=sharing&ouid=116672064887999106426&rtpof=true&sd=true)


# Ownership
The software and all of the supporting materials must be delivered to the client as a condition of completion of the project. There are no formal agreements as to the ownership of the software. However, if the client requires clear and legal title to the software (or some other type of arrangement), a separate agreement should be prepared by the client with the team member signatures provided as an agreement to the client’s “terms and conditions” regarding ownership.